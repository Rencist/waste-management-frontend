export type SignUp = {
  name: string;
  email: string;
  password: string;
};

export type LogIn = {
  email: string;
  password: string;
};
