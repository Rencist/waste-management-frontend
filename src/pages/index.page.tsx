import withAuth from '@/components/hoc/withAuth';
import SignUpPage from '@/pages/signup.page';

export default withAuth(DashboardPage, ['all']);

function DashboardPage() {
  return <SignUpPage />;
}
