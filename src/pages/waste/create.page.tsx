import { useMutation, useQuery } from '@tanstack/react-query';
import { AxiosError, AxiosResponse } from 'axios';
import { useRouter } from 'next/router';
import { useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';

import Button from '@/components/buttons/Button';
import Input from '@/components/form/Input';
import SearchableSelectInput from '@/components/form/SearchableSelectInput';
import UnstyledLink from '@/components/links/UnstyledLink';
import SEO from '@/components/SEO';
import Typography from '@/components/typography/Typography';
import Layout from '@/layouts/Layout';
import api from '@/lib/api';
import { ApiError, ApiReturn } from '@/types/api';
import { Waste } from '@/types/entity/waste';

export default function CreateWastePage() {
  const [error, setError] = useState('');
  const router = useRouter();

  const methods = useForm<Waste>();
  const { handleSubmit } = methods;

  const { mutate: handleWaste, isLoading } = useMutation<
    AxiosResponse<ApiReturn<Waste>> | void,
    AxiosError<ApiError>,
    Waste
  >(async (data: Waste) => {
    const res = await api.post('/waste', data);
    return res;
  });

  const type = useQuery<
    ApiReturn<{ id: string; material: string; name: string }[]>
  >(['/type']);

  const onSubmit = (data: Waste) => {
    handleWaste(
      {
        type_id: data.type_id,
        name: data.name,
      },
      {
        onSuccess: () => router.push('/waste'),
        onError: (err) => {
          err.response && setError(err.response?.data.message);
        },
      }
    );
  };

  return (
    <Layout>
      <SEO title='Input Sampah' description='Input Sampah Page' />

      <main className='flex min-h-screen justify-center w-full bg-base-surface'>
        <section className='flex items-center justify-center w-full md:w-1/3 md:min-w-[400px] px-8 py-12'>
          <FormProvider {...methods}>
            <form
              className='w-full max-w-[400px] flex flex-col gap-6'
              onSubmit={handleSubmit(onSubmit)}
            >
              <Typography
                font='montserrat'
                variant='h3'
                weight='bold'
                className='text-teal-600 text-center'
              >
                Input Sampah
              </Typography>

              <SearchableSelectInput
                id='type_id'
                label='Kategori Sampah'
                placeholder='Pilih Kategori Sampah'
                validation={{ required: 'Kategori Sampah tidak boleh kosong!' }}
                options={
                  type?.data?.data.map((val) => ({
                    value: val.id,
                    label: val.name,
                  })) || []
                }
              />

              <div className='space-y-3'>
                <Input
                  id='name'
                  label='Nama Sampah'
                  placeholder='Masukkan Nama Sampah'
                  validation={{ required: 'Nama Sampah harus diisi' }}
                />
              </div>

              <div className='flex flex-col items-center gap-1.5 pt-2'>
                {error && (
                  <Button
                    size='small'
                    variant='danger'
                    className='pointer-events-none w-full bg-opacity-80'
                  >
                    {error}
                  </Button>
                )}
                <Button
                  type='submit'
                  className='w-full'
                  textClassName='font-secondary'
                  isLoading={isLoading}
                >
                  Simpan
                </Button>

                <div className='flex gap-1'>
                  <UnstyledLink
                    href='/waste'
                    className='self-end underline text-teal-400 hover:text-teal-600'
                  >
                    <Typography font='open-sans' variant='c' weight='semibold'>
                      Cek data sampah
                    </Typography>
                  </UnstyledLink>
                </div>
              </div>
            </form>
          </FormProvider>
        </section>
      </main>
    </Layout>
  );
}
