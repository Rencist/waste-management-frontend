import { useQuery } from '@tanstack/react-query';

import Button from '@/components/buttons/Button';
import UnstyledLink from '@/components/links/UnstyledLink';
import SEO from '@/components/SEO';
import Typography from '@/components/typography/Typography';
import Layout from '@/layouts/Layout';
import WasteCard from '@/pages/waste/container/WasteCard';
import { ApiReturn } from '@/types/api';

export default function SignUpPage() {
  const userWaste = useQuery<
    ApiReturn<{ id: string; material: string; type: string; waste: string }[]>
  >(['/waste']);

  return (
    <Layout>
      <SEO title='Data Sampah' description='Data Sampah Page' />

      <main className='flex min-h-screen justify-center w-full bg-base-surface'>
        <section className='flex items-center justify-center w-full md:w-1/3 md:min-w-[400px] px-8 py-12'>
          <div className='flex flex-col gap-6'>
            <Typography
              font='montserrat'
              variant='h3'
              weight='bold'
              className='text-teal-600 text-center'
            >
              Data Sampah
            </Typography>
            <div className='flex flex-col items-center gap-1.5'>
              <UnstyledLink href='/waste/create' className='w-full'>
                <Button
                  type='submit'
                  className='w-full'
                  textClassName='font-secondary'
                >
                  Tambah
                </Button>
              </UnstyledLink>
            </div>
            <div className='space-y-3'>
              {userWaste?.data?.data.map((waste) => (
                <WasteCard
                  key={waste.id}
                  id={waste.id}
                  name={waste.waste}
                  type={waste.type}
                />
              ))}
            </div>
          </div>
        </section>
      </main>
    </Layout>
  );
}
