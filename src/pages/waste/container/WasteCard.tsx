import { useMutation } from '@tanstack/react-query';
import { AxiosError, AxiosResponse } from 'axios';
import router from 'next/router';
import { useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { HiOutlineExclamationCircle } from 'react-icons/hi';

import Button from '@/components/buttons/Button';
import Typography from '@/components/typography/Typography';
import api from '@/lib/api';
import { ApiError, ApiReturn } from '@/types/api';

type WasteCardProps = {
  id: string;
  name: string;
  type: string;
};

type DeleteWaste = {
  waste_id: string;
};

export default function WasteCard({ id, name, type }: WasteCardProps) {
  const methods = useForm<DeleteWaste>();
  const { handleSubmit } = methods;

  const { mutateAsync: handleDelete, isLoading } = useMutation<
    AxiosResponse<ApiReturn<null>> | void,
    AxiosError<ApiError>,
    DeleteWaste
  >(async (data: DeleteWaste) =>
    api.delete('/waste?waste_id=' + data.waste_id)
  );

  const [error, setError] = useState('');
  const onSubmit = (data: DeleteWaste) => {
    handleDelete(
      {
        waste_id: data.waste_id,
      },
      {
        onSuccess: () => router.reload(),
        onError: (err) => {
          err.response && setError(err.response?.data.errors);
        },
      }
    );
  };

  return (
    <div className='w-full shadow-md bg-base-light rounded-xl overflow-hidden'>
      <div className='space-y-3'>
        <div className='flex flex-col gap-3 bg-base-surface p-3 rounded-lg'>
          <div className='flex flex-col gap-4'>
            <div>
              <Typography
                variant='p'
                weight='bold'
                className='text-teal-600 capitalize'
              >
                {name}
              </Typography>
            </div>
            <div className='flex justify-between'>
              <div>
                <Typography variant='c' className='text-base-icon capitalize'>
                  {type}
                </Typography>
              </div>
              <div>
                <FormProvider {...methods}>
                  <form
                    onSubmit={handleSubmit(
                      onSubmit.bind(null, { waste_id: id })
                    )}
                    className=' w-full flex flex-row gap-4'
                  >
                    <Button
                      type='submit'
                      variant='unstyled'
                      className='w-full'
                      isLoading={isLoading}
                    >
                      <Typography
                        variant='c'
                        className='text-red-300 capitalize'
                      >
                        Delete
                      </Typography>
                    </Button>
                    {error && (
                      <div className='flex space-x-1'>
                        <HiOutlineExclamationCircle className='shrink-0 text-red-200' />
                        <Typography
                          variant='c'
                          weight='semibold'
                          className='text-red-200'
                        >
                          {error}
                        </Typography>
                      </div>
                    )}
                  </form>
                </FormProvider>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
